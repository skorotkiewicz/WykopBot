WykopBot ver0.1a - twój bot na Wykop.pl
---------------

Prosty bot z wieloma przydatnymi funkcjami które umilą Ci używanie największego portalu informacyjnego w polsce.

Najlepsze opcje:

  - Proste pisanie wpisów na mikroblog z Twojego komunikatora
  - Sprawdzanie statusow
  - Funkcje szukania

Jeżeli chcesz współtwórczyć projekt, będę Ci za to bardzo wdzięczny jak i użytkownicy Wykopu, aby zacząć zapoznaj się z 
http://www.wykop.pl/dla-programistow/dokumentacja/  
Wykopu, gdy już bedziesz gotowy, pobierz z Gita źródła i zacznij współtwórczyć :)

WykopBot GG pod numerem 48248202 i na XMPP pod adresem wykop@jabbim.cz
Strona WykopBot --> http://wykopbot.spytajsie.com/


  [dokumentacją]: http://www.wykop.pl/dla-programistow/dokumentacja/
  [web]: http://wykopbot.spytajsie.com/

