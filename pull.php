<?php
ob_start();
require_once ('libs/wykop.class.php');
require_once ('libs/ggbot/MessageBuilder.php');
require_once ('libs/db.php');

$wapi = new libs_Wapi($appKey, $secretKey);
$M = new MessageBuilder();

$userGG = cleanText($_GET['from']);
$userMG = $HTTP_RAW_POST_DATA;
$msg = explode(' ', $userMG);
$komenda = $msg[0];
$komenda2 = $msg[1];
$time = time();

$sql = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'");
foreach ($sql as $d) {
    $token = $d['token'];
    $login = $d['login'];
    $userkey = $d['userkey'];
    $last_visit = $d['last_visit'];
}
if ($userMG == 'wykop loguj') {
    $checkUser = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'")->rowCount();
    if ($checkUser == 0) {
        $ConnectUrl = $wapi->getConnectUrl('http://wykopbot.spytajsie.com/wykop.php?gg=' . $userGG); // działa
        $M->addText('Aby się zarejestrować na WykopBot wystarczy kliknąć na ten link ' . $ConnectUrl . ' po rejestracji bedziesz mógł korzystać z wszystkich opcji bota');
        $userData = $wapi->handleConnectData();
    } else {
        $M->addText('Jesteś już zarejestrowany pomyślnie, wpisz pomoc aby użyskać więcej informacji');
    }
} elseif ($komenda == 'mb') {
    // XXX wysyłanie wiadomosci na mikroblog
    // uzywanie 24 godzinnego userkey'a
    $checkUser = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'")->rowCount();
    if ($checkUser == 1) {
        if ($last_visit >= time() - 72000) {
            $wapi->setUserKey($userkey);
            $wapi->doRequest("entries/add", array('body' => substr($userMG, 2)));
        } else {
            $dane = $wapi->loguj($login, $token, $appKey);
            $dane = json_decode($dane['content']);
            $wapi->setUserKey($dane->userkey);
            $db->query("UPDATE `users` SET `userkey` = '" . $dane->userkey . "', `last_visit` = '$time' WHERE `gadugadu` = '$userGG';");
            $wapi->doRequest("entries/add", array('body' => substr($userMG, 2)));
        }
        $M->addText('Wysłano http://www.wykop.pl/mikroblog/moje/');
    } else {
        $M->addText('Zaloguj się aby móc mieć dostęp do tej opcji, w tym celu wpisz polecenie "wykop loguj".');
    }
} elseif ($komenda == 'pi') {
    $checkUser = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'")->rowCount();
    if ($checkUser == 1) {
        // XXX sprawdzanie ostatnich powiadomień
        // uzywanie 24 godzinnego userkey'a
        if ($last_visit >= time() - 72000) {
            $wapi->setUserKey($userkey);
            $result = $wapi->doRequest("Mywykop/Notifications");
            $resultCount = $wapi->doRequest("Mywykop/NotificationsCount");
        } else {
            $dane = $wapi->loguj($login, $token, $appKey);
            $dane = json_decode($dane['content']);
            $wapi->setUserKey($dane->userkey);
            $db->query("UPDATE `users` SET `userkey` = '" . $dane->userkey . "', `last_visit` = '$time' WHERE `gadugadu` = '$userGG';");
            $result = $wapi->doRequest("Mywykop/Notifications");
            $resultCount = $wapi->doRequest("Mywykop/NotificationsCount");
        }
        $M->addText('Ostatnie Twoje powiadomienia (' . $resultCount['count'] . ' nieprzeczytanych) http://www.wykop.pl/powiadomienia/do-mnie/
	');
        foreach ($result as $r) {
            $powiadomienia = "{$r['date']} - @{$r['author']}: {$r['body']}
	";
            $M->addText($powiadomienia);
        }
    } else {
        $M->addText('Zaloguj się aby móc mieć dostęp do tej opcji, w tym celu wpisz polecenie "wykop loguj".');
    }
} elseif ($komenda == 'pih') {
    $checkUser = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'")->rowCount();
    if ($checkUser == 1) {
        // XXX sprawdzanie ostatnich hash tagi powiadomień
        // uzywanie 24 godzinnego userkey'a
        if ($last_visit >= time() - 72000) {
            $wapi->setUserKey($userkey);
            $result = $wapi->doRequest("Mywykop/HashTagsNotifications");
            $resultCount = $wapi->doRequest("Mywykop/HashTagsNotificationsCount");
        } else {
            $dane = $wapi->loguj($login, $token, $appKey);
            $dane = json_decode($dane['content']);
            $wapi->setUserKey($dane->userkey);
            $db->query("UPDATE `users` SET `userkey` = '" . $dane->userkey . "', `last_visit` = '$time' WHERE `gadugadu` = '$userGG';");
            $result = $wapi->doRequest("Mywykop/HashTagsNotifications");
            $resultCount = $wapi->doRequest("Mywykop/HashTagsNotificationsCount");
        }
        $M->addText('Ostatnie Twoje powiadomienia (' . $resultCount['count'] . ' nieprzeczytanych) http://www.wykop.pl/powiadomienia/tagi/
	');
        foreach ($result as $r) {
            $powiadomienia = "{$r['date']} - @{$r['author']}: {$r['body']}
	";
            $M->addText($powiadomienia);
        }
    } else {
        $M->addText('Zaloguj się aby móc mieć dostęp do tej opcji, w tym celu wpisz polecenie "wykop loguj".');
    }
} elseif ($komenda == 'help') {
    $checkUser = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'")->rowCount();
    if ($checkUser == 1) {
        $M->addText('
  * powiadomienia na gg *
    - polecenie "pi" sprawdza Twoje ostatnie powiadomienia (bez powiadomieniach o tagach)
    - polecenie "pih" sprawdza Twoje ostatnie powiadomienia o tagach (tylko powiadomieniach o tagach)

  * mikroblog *
    - gdy chcesz napisać nową wiadomość na Mikroblogu z pomoca przyjdzie polecenie "mb" a sposób użycia jest prosty "mb <twoja treść>", przykład: "mb cześć mikroby"

  * profil *
    - wszystkie ważne dla Ciebie informacje o Twoim profilu znajdziesz wpisująć polecenie "profil"

  * szukanie *
    - zawsze pod ręką, zawsze przy Tobie, szukaj potrzebnych materiałów z wykop, polecenia 
       Wyszukiwanie znalezisk "szkj urls <szukana fraza>"
       Wyszukiwanie wpisów z mikroblogu "szkj mb <szukana fraza>"
       Wyszukiwanie profili "szkj profile <szukana fraza>"

-----------------------------------------
  * usunięcie powiązania bota *
    - jeżeli nie chcesz korzystać z WykopBot\'a na swoim GG, wpisz polecenie "rmac__cou_erq_r__' . strtolower(cleanText($login)) . '_bjpt", zostanie usunięte powiązanie bota z Wykopem, w każdym momencie możesz się ponownie zarejestrować do usługi WykopBot! :) 
');
    } else {
        $M->addText('Zaloguj się aby móc mieć dostęp do tej opcji, w tym celu wpisz polecenie "wykop loguj".');
    }
} elseif ($komenda == 'rmac__cou_erq_r__' . strtolower(cleanText($login)) . '_bjpt') {
    $db->query("DELETE FROM `users` WHERE `gadugadu` = $userGG");
    $M->addText('Powiązanie usunięte.');
} elseif ($komenda == 'profil') {
    $checkUser = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'")->rowCount();
    if ($checkUser == 1) {
        if ($last_visit >= time() - 72000) {
            $wapi->setUserKey($userkey);
            $NotificationsCount = count($wapi->doRequest("Mywykop/NotificationsCount")) - 1;
            $HashTagCount = count($wapi->doRequest("Mywykop/HashTagsNotificationsCount")) - 1;
            $Rank = $wapi->doRequest("Profile/Index/$login");
        } else {
            $dane = $wapi->loguj($login, $token, $appKey);
            $dane = json_decode($dane['content']);
            $wapi->setUserKey($dane->userkey);
            $db->query("UPDATE `users` SET `userkey` = '" . $dane->userkey . "', `last_visit` = '$time' WHERE `gadugadu` = '$userGG';");
            $NotificationsCount = count($wapi->doRequest("Mywykop/NotificationsCount")) - 1;
            $HashTagCount = count($wapi->doRequest("Mywykop/HashTagsNotificationsCount")) - 1;
            $Rank = $wapi->doRequest("Profile/Index/$login");
        }
        $M->addText("
Profil: $login
Liczba powiadomień: $NotificationsCount
Liczba powiadomień tagi: $HashTagCount
Miejsce w rankingu: {$Rank['rank']}

Obserwujący: {$Rank['followers']}
Obserwowani: {$Rank['following']}

Ilość dodanych linków: {$Rank['links_added']}
Ilość opublikowanych linków: {$Rank['links_published']}
Ilość napisanych komentarzy: {$Rank['comments']}
Ilość wpisów: {$Rank['entries']}
Ilość wykopanych linków: {$Rank['diggs']}
ilość linków zakopanych: {$Rank['buries']}
");
    } else {
        $M->addText('Zaloguj się aby móc mieć dostęp do tej opcji, w tym celu wpisz polecenie "wykop loguj".');
    }
} elseif ($komenda == 'szkj') {
    $checkUser = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'")->rowCount();
    if ($checkUser == 1) {
        if ($last_visit >= time() - 72000) {
            $wapi->setUserKey($userkey);
            if ($komenda2 == 'urls') {
                $Index = $wapi->doRequest("Search/Links", array('q' => substr($userMG, 10)));
            } // Wyszukiwanie znalezisk
            elseif ($komenda2 == 'mb') {
                $Index = $wapi->doRequest("Search/Entries", array('q' => substr($userMG, 6)));
            } // Wyszukiwanie wpisów z mikroblogu
            elseif ($komenda2 == 'profile') {
                $Index = $wapi->doRequest("Search/Profiles", array('q' => substr($userMG, 13)));
            } else {
            } // Wyszukiwanie profili
            
        } else {
            $dane = $wapi->loguj($login, $token, $appKey);
            $dane = json_decode($dane['content']);
            $wapi->setUserKey($dane->userkey);
            $db->query("UPDATE `users` SET `userkey` = '" . $dane->userkey . "', `last_visit` = '$time' WHERE `gadugadu` = '$userGG';");
            if ($komenda2 == 'urls') {
                $Index = $wapi->doRequest("Search/Links", array('q' => substr($userMG, 10)));
            } // Wyszukiwanie znalezisk
            elseif ($komenda2 == 'mb') {
                $Index = $wapi->doRequest("Search/Entries", array('q' => substr($userMG, 6)));
            } // Wyszukiwanie wpisów z mikroblogu
            elseif ($komenda2 == 'profile') {
                $Index = $wapi->doRequest("Search/Profiles", array('q' => substr($userMG, 13)));
            } else {
            } // Wyszukiwanie profili
            
        }
        foreach ($Index as $r) {
            echo $r['title'] . ' \ ' . $r['url'] . '
';
        }
    } else {
        $M->addText('Zaloguj się aby móc mieć dostęp do tej opcji, w tym celu wpisz polecenie "wykop loguj".');
    }
} else {
    $checkUser = $db->query("SELECT * FROM `users` WHERE `gadugadu` = '$userGG'")->rowCount();
    if ($checkUser == 0) {
        $M->addText('Widzę, że jesteś nowy, aby się zarejestrować wystarczy wpisać polecenie "wykop loguj"'); //  a po rejestracji będziesz miał więcej możliwości niż sam prezydent stanów zjednoczonych
        
    } else {
        $M->addText('
  * powiadomienia na gg *
    - polecenie "pi" sprawdza Twoje ostatnie powiadomienia (bez powiadomieniach o tagach)
    - polecenie "pih" sprawdza Twoje ostatnie powiadomienia o tagach (tylko powiadomieniach o tagach)

  * mikroblog *
    - gdy chcesz napisać nową wiadomość na Mikroblogu z pomoca przyjdzie polecenie "mb" a sposób użycia jest prosty "mb <twoja treść>", przykład: "mb cześć mikroby"

  * profil *
    - wszystkie ważne dla Ciebie informacje o Twoim profilu znajdziesz wpisująć polecenie "profil"

  * szukanie *
    - zawsze pod ręką, zawsze przy Tobie, szukaj potrzebnych materiałów z wykop, polecenia 
       Wyszukiwanie znalezisk "szkj urls <szukana fraza>"
       Wyszukiwanie wpisów z mikroblogu "szkj mb <szukana fraza>"
       Wyszukiwanie profili "szkj profile <szukana fraza>"

  * więcej pomocy * (tylko dla zalogowanych)
    - wpisz polecenie "help"');
    }
}
$M->reply();

