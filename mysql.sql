--
-- Baza danych: `wykopbot`
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `userkey` varchar(255) NOT NULL,
  `last_visit` varchar(255) NOT NULL,
  `join_time` varchar(255) NOT NULL,
  `gadugadu` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
